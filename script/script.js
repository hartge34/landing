window.onload=function(){
    var menuBut = document.querySelector('#butter');

    menuBut.addEventListener('click', function(elem){
        elem.preventDefault();
        var menuToggle = document.querySelector('.hidden-nav-list');

        if(menuToggle.style.display == "block"){
            menuToggle.style = "display: none;";
        } else {
            menuToggle.style = "display: block;";
        }

    });

    var butShow = document.querySelector('#butShow');

    butShow.addEventListener('click', function(elem){
        elem.preventDefault();
        var modal = document.querySelector('#pShow');
        if(modal.style.display == "block"){
            modal.style = "display: none;";
        } else {
            modal.style = "display: block;";
        }

    });

    var footerButShow = document.querySelector('#footerButShow');

    footerButShow.addEventListener('click', function(elem){
        elem.preventDefault();
        var modal = document.querySelector('#footerpShow');
        if(modal.style.display == "block"){
            modal.style = "display: none;";
        } else {
            modal.style = "display: block;";
        }

    });
};